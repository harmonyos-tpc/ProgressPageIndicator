package com.cepheuen.app;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.utils.Color;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import com.cepheuen.progresspageindicator.ProgressPageIndicator;

public class ExampleOhosTest {
    ProgressPageIndicator progress;
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.cepheuen.app", actualBundleName);
    }
    @Test
    public void testradius(){
        try {
            progress.setRadius(20);
        } catch (RuntimeException e) {
            assertTrue("Radius not set",true);
        }
    }

    @Test
    public void testColor(){
        try {
            progress.setFillColor(Color.BLACK);
        } catch (RuntimeException e) {
            assertTrue("Color not set",true);
        }
    }

    @Test
    public void testStrokeColor(){
        try {
            progress.setStrokeColor(Color.BLACK);
        } catch (RuntimeException e) {
            assertTrue("Stroke not set",true);
        }
    }

    @Test
    public void testDotgap(){
        try {
            progress.setDotGap(20);
        } catch (RuntimeException e) {
            assertTrue("Dotgap not set",true);
        }
    }

    @Test
    public void testStrokeRadius(){
        try {
            progress.setStrokeRadius(20);
        } catch (RuntimeException e) {
            assertTrue("setStrokeRadius not set",true);
        }
    }
}