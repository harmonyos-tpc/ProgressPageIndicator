package com.cepheuen.app;

import ohos.agp.components.AttrSet;
import ohos.agp.components.PageSlider;
import ohos.app.Context;

public class NonSwipeableViewPager extends PageSlider {

    public NonSwipeableViewPager(Context context) {
        super(context);
    }

    public NonSwipeableViewPager(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public NonSwipeableViewPager(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

}