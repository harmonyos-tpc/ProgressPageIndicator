/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cepheuen.app.slice;

import com.cepheuen.app.NonSwipeableViewPager;
import com.cepheuen.app.ResourceTable;
import com.cepheuen.progresspageindicator.ProgressPageIndicator;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.agp.components.Image;
import ohos.agp.components.Button;
import ohos.agp.components.PageSliderProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

import java.util.List;

public class PagerAbilitySlice extends AbilitySlice {

    private String[] TITLES = {"Sky 1", "Sky 2", "Sky 3", "Sky 4", "Sky 5"};
    private final int[] IMGS = {ResourceTable.Media_sky1, ResourceTable.Media_sky2, ResourceTable.Media_sky3, ResourceTable.Media_sky4, ResourceTable.Media_sky5, ResourceTable.Media_skydef};

    private ProgressPageIndicator dots_layout;
    private NonSwipeableViewPager viewPager;
    private Text title;
    private Button next, prev;

    private int maxPages = TITLES.length;
    private int currentPage = 0;
    PageSliderProvider mAdapter =null;


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_activity_pager);
        title = (Text) findComponentById(ResourceTable.Id_title);
        next = (Button) findComponentById(ResourceTable.Id_next);
        prev = (Button) findComponentById(ResourceTable.Id_prev);
        dots_layout = (ProgressPageIndicator) findComponentById(ResourceTable.Id_dotsL);
        viewPager = (NonSwipeableViewPager) findComponentById(ResourceTable.Id_viewpager);
        mAdapter = new PagerAdapter(getApplicationContext());
        viewPager.setProvider( mAdapter);

        dots_layout.setViewPager(viewPager);
        title.setText(TITLES[currentPage]);

        next.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                currentPage++;
                if (currentPage < maxPages) {
                    viewPager.setProvider( null);
                    viewPager.setProvider( mAdapter);
                    viewPager.setCurrentPage(currentPage, true);
                    title.setText(TITLES[currentPage]);

                }
                else {
                    currentPage = maxPages - 1;
                    String[] newArray = new String[TITLES.length + 1];
                    System.arraycopy(TITLES, 0, newArray, 0, TITLES.length);
                    newArray[maxPages] = "Added Page " + (maxPages + 1);
                    TITLES = newArray;
                    maxPages = maxPages + 1;
                    viewPager.setProvider( null);
                    viewPager.setProvider( mAdapter);
                    viewPager.setCurrentPage(currentPage, true);
                    ToastDialog toastDialog = new ToastDialog(getContext());
                    toastDialog.setText("End of pages");
                    toastDialog.setAlignment(LayoutAlignment.HORIZONTAL_CENTER | LayoutAlignment.BOTTOM);
                    toastDialog.setDuration(1000);// 让Toast显示为我们自定义的样子
                    toastDialog.show();
                    dots_layout.notifyDataSetChanged();
                }
            }
        });

        prev.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                currentPage--;
                if (currentPage >= 0) {
                    viewPager.setCurrentPage(currentPage, true);
                    title.setText(TITLES[currentPage]);
                } else {
                    currentPage = 0;
                    ToastDialog toastDialog = new ToastDialog(getContext());
                    toastDialog.setText("Start of pages");
                    toastDialog.setAlignment(LayoutAlignment.HORIZONTAL_CENTER | LayoutAlignment.BOTTOM);
                    toastDialog.setDuration(1000);// 让Toast显示为我们自定义的样子
                    toastDialog.show();
                }
            }
        });
    }

    public class PagerAdapter extends PageSliderProvider {
        private LayoutScatter mLayoutInflater;

        public PagerAdapter(Context context) {
            super();
            mLayoutInflater = LayoutScatter.getInstance(context);
        }

        @Override
        public String getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        private List<Component> fragments;

        public PagerAdapter(List<Component> list) {
            this.fragments = list;
        }


        @Override
        public Object createPageInContainer(ComponentContainer container, int position) {
            final Component view = mLayoutInflater.parse(ResourceTable.Layout_pager_item, container, false);

            final Image img = (Image) view.findComponentById(ResourceTable.Id_img_item);
            int imageid;

            try {
                imageid = IMGS[position];
            } catch (ArrayIndexOutOfBoundsException e) {
                imageid = ResourceTable.Media_skydef;
            }

            img.setPixelMap(imageid);
            img.setScaleMode(Image.ScaleMode.CLIP_CENTER);
            container.addComponent(view);
            return view;
        }

        @Override
        public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {

        }

        @Override
        public boolean isPageMatchToObject(Component component, Object o) {
            return false;
        }

    }

}
