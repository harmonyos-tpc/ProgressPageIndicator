package com.cepheuen.progresspageindicator;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.PageSlider;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * Draws circles (one for each view). Views till the position is filled and
 * others are only stroked.
 */
public class ProgressPageIndicator extends DirectionalLayout implements PageSlider.PageChangedListener {
    private float mRadius = 20;
    private float mStrokeSize = 2;
    private float mDotGap = 10;

    private int mCurrentPage, mMaxPage;
    private int mCurrentPageTemp = -1;
    private Color mStrokeColor ;
    private Color mFillColor ;

    static float mRadius_default = 20;
    static float mStrokeSize_default = 2;
    static float mDotGap_default = 10;
    static Color mStrokeColor_default = new Color(0xffffffff);
    static Color mFillColor_default = new Color(0xffffffff);

    private ShapeElement mCircleDrawable;
    private ShapeElement mStrokeDrawable;

    private PageSlider mViewPager;

    public ProgressPageIndicator(Context context) {
        super(context);
    }

    public ProgressPageIndicator(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context,attrSet);
        }

    private void init(Context context, AttrSet attrSet) {
        mRadius = attrSet.getAttr("radius").isPresent() ? attrSet.getAttr("radius").get().getDimensionValue():mRadius_default;
        mDotGap = attrSet.getAttr("dotGap").isPresent() ? attrSet.getAttr("dotGap").get().getDimensionValue():mDotGap_default;
        mStrokeSize = attrSet.getAttr("strokeRadius").isPresent() ? attrSet.getAttr("strokeRadius").get().getDimensionValue():mStrokeSize_default;
        mStrokeColor = attrSet.getAttr("strokeColor").isPresent() ? attrSet.getAttr("strokeColor").get().getColorValue():mStrokeColor_default;
        mFillColor = attrSet.getAttr("fillColor").isPresent() ? attrSet.getAttr("fillColor").get().getColorValue():mFillColor_default;
    }

    public ProgressPageIndicator(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
  }

    /**
     * Sets the radius of the dot indicator
     *
     * @param radius Radius of the dot indicator
     */
    public void setRadius(int radius) {
        this.mRadius = radius;
        invalidate();
    }

    /**
     * Sets the radius of the stroke
     *
     * @param size
     */
    public void setStrokeRadius(int size) {
        this.mStrokeSize = size;
        invalidate();
    }

    /**
     * Sets the gap between the indicators
     *
     * @param dotGap Gap between the indicators
     */
    public void setDotGap(int dotGap) {
        this.mDotGap = dotGap;
        invalidate();
    }

    /**
     * Sets the fill color of the indicator
     *
     * @param fillColor Fill color of the indicator
     */
    public void setFillColor(Color fillColor) {
        this.mFillColor = fillColor;
        invalidate();
    }

    /**
     * Sets the stroke color of the unfilled indicator
     *
     * @param strokeColor Stroke color of the unfilled indicator
     */
    public void setStrokeColor(Color strokeColor) {
        this.mStrokeColor = strokeColor;
        invalidate();
    }

    /**
     * Sets the PageSlider to the indicator
     *
     * @param viewPager Viewpager
     */
    public void setViewPager(PageSlider viewPager) {
        if (mViewPager == viewPager) {
            return;
        }
        if (mViewPager != null) {
            mViewPager.addPageChangedListener(null);
        }
        if (viewPager.getProvider() == null) {
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }
        this.mViewPager = viewPager;
        this.mViewPager.addPageChangedListener(this);
        this.mMaxPage = mViewPager.getProvider().getCount();

        if(mCurrentPageTemp != -1)
            setCurrentPage(mCurrentPageTemp);

        initialize();
    }

    /**
     * Sets the viewpager to the indicator and points to the specified position
     *
     * @param viewPager       ViewPager
     * @param initialPosition Default position
     */
    public void setViewPager(PageSlider viewPager, int initialPosition) {
        setViewPager(viewPager);
        setCurrentPage(initialPosition);
        initialize();
    }

    /**
     * Draws the filled and the stroked indicators
     */
    private void drawIndicators() {
        // strokeRadius should not be grater than radius
        if(mStrokeSize >= mRadius)
            mStrokeSize = mRadius - 1;

        mCircleDrawable = new ShapeElement();
        mCircleDrawable.setShape(ShapeElement.OVAL);
        mCircleDrawable.setRgbColor(new RgbColor(RgbColor.fromArgbInt(mFillColor.getValue())));
        mCircleDrawable.setBounds(0, 0, (int) mRadius,(int) mRadius);

        mStrokeDrawable = new ShapeElement();
        mStrokeDrawable.setShape(ShapeElement.OVAL);
        mStrokeDrawable.setRgbColor(new RgbColor(255,255,255,0));
        mStrokeDrawable.setStroke((int) mStrokeSize, new RgbColor(RgbColor.fromArgbInt(mStrokeColor.getValue())));
        mStrokeDrawable.setBounds(0, 0, (int) mRadius,(int) mRadius);
    }


    /**
     * Sets the current page
     *
     * @param page Current page
     */
    public void setCurrentPage(int page) {
        if(page >= mMaxPage)
            if(mMaxPage == 0)
                mCurrentPageTemp = page;
            else
                mCurrentPage = mMaxPage - 1;
        else
            mCurrentPage = page;

        invalidate();
    }

    /**
     * Used to notify the data set change to the indicator
     */
    public void notifyDataSetChanged() {
        mMaxPage = this.mViewPager.getProvider().getCount();
    }

    private void initialize() {
        prepareDots();
        fillDots();
    }

    @Override
    public void invalidate() {
        super.invalidate();
        drawIndicators();
        initialize();
    }

    /**
     * Preparing the canvas with the indicators
     */
    private void prepareDots() {
        removeAllComponents();
        for (int dotLoop = 0; dotLoop < mMaxPage; dotLoop++) {
            Image imageView = new Image(getContext());
            LayoutConfig params = new LayoutConfig((int) mRadius, (int) mRadius);
            if (dotLoop != 0) {
                if (getOrientation() == HORIZONTAL)
                    params.setMargins((int) mDotGap, 0, 0, 0);
                else
                    params.setMargins(0, (int) mDotGap, 0, 0);
            }

            imageView.setLayoutConfig(params);
            imageView.setImageElement(mStrokeDrawable);
            imageView.setId(dotLoop);
            addComponent(imageView);
        }
    }

    /**
     * Filling the canvas with the indicators
     */
    private void fillDots() {
        drawIndicators();

        // mCurrentPage should not be greater total pages
        if(mCurrentPage >= mMaxPage)
            mCurrentPage = mMaxPage - 1;

        for (int fillLoop = 0; fillLoop <= mCurrentPage; fillLoop++) {
            Image image = ((Image) findComponentById(fillLoop));
            if(image != null)
                image.setImageElement(mCircleDrawable.getCurrentElement());
        }
        for (int unFillLoop = mCurrentPage + 1; unFillLoop < mMaxPage; unFillLoop++) {
            Image image = ((Image) findComponentById(unFillLoop));
            if(image != null)
                image.setImageElement(mStrokeDrawable.getCurrentElement());
        }
    }

    @Override
    public void onPageSliding(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageChosen(int position) {
        mCurrentPage = position;
        mMaxPage = this.mViewPager.getProvider().getCount();
        initialize();
    }

    @Override
    public void onPageSlideStateChanged(int state) {

    }
}
