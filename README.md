## ProgressPageIndicator

## Introduction
An openharmony library that provides a filling Page Indicator for viewpagers.

## Usage instructions

Include the ProgressPageIndicator widget in your layout.
```
        <com.cepheuen.progresspageindicator.ProgressPageIndicator
            ohos:id="$+id:dotsL"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:below="@+id/title"
            ohos:horizontal_center="true"
            ohos:top_margin="5vp"
            app:dotGap="2dp"
            app:fillColor="#cccccc"
            app:radius="8dp"/>
```
Refer the widget from the layout and set the viewpager to it by using setViewPager().
```
dots_layout = (ProgressPageIndicator) findComponentById(ResourceTable.Id_dotsL);
dots_layout.setViewPager(viewPager);
```			

```
## Installation instruction
**Method 1:**
Generate the .har package through the library and add the .har package to the libs folder.
Add the following code to the entry gradle:
```
implementation fileTree  (dir: 'libs', include: ['*.jar', '*.har'])
```
**Method 2:**
Add the following code to the entry gradle:
```
implementation project(path: ':progresspageindicator')
```
**Method 3:**
For using ProgressPageIndicator from remote repository in separate application, add the below dependency in "entry" build.gradle.

Modify entry build.gradle as below :

```
dependencies {
  implementation 'io.openharmony.tpc.thirdlib:ProgressPageIndicator:1.0.0'
}
```

##License
```
Copyright 2015 Muthuramakrishnan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```